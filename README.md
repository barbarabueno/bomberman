# :bomb: :video_game: Bomberman 



## :pencil: Descrição

Iremos recriar, utilizando a linguagem de programação Java, o clássico jogo Bomberman, conhecido por um personagem que deve ir soltando bombas e derrotando "monstros" para conseguir passar de fase e pontuar. 

A imagem abaixo é um exemplo do jogo já existente, mas que queremos adaptar para um cenário que remeta a lugares da USP.

![My Image](./images/readme/bomberman.jpeg)

## :page_with_curl: Requisitos/Etapas

- 1: Desenvolver o mapa do jogo, que corresponderá a uma área retangular fechada, com blocos fixos e indestrutíveis e blocos com posições aleatórias e destrutíveis com a bomba. Os cenários serão temáticos de acordo com lugares da USP.

- 2: Adicionar o controle do bomberman, por meio de inputs do teclado o jogador poderá se movimentar para cima, baixo, esquerda e direita, além de ter um botão para soltar e para empurrar a bomba.

- 3: A arena será populada por diversos "monstros", os quais o jogador deve derrotar. Haverá diversos tipos de monstros: 

  - Monstros que andam sempre em uma mesma linha;
  - Monstros que seguem o jogador;
  - Monstros que atravessam as bombas;
  - Outros tipos que desenvolveremos ao longo do projeto. 
  
  Caso o jogador encoste em um dos monstros, ele perde uma de suas vidas e é regenerado em sua posição inicial. Perdendo todas as vidas, ele deve recomeçar o jogo e perde todos os pontos.
Além disso, haverá power ups escondidos nos blocos destrutíveis para que o jogador consiga modificar suas habilidades.

- 4: Utilizando o padrão Fábrica Abstrata podemos implementar diversos cenários de arena e blocos. Dessa forma, o jogador conseguirá alterar o visual do jogo dentro das opções fornecidas no padrão.

- 5: Utilizando o padrão State podemos criar diferentes dificuldades, a partir dos monstros.

- 6: Implementar efeitos sonoros para as diferentes fases, além de eventos como receber um power up ou explodir uma bomba.

## Entregas

- Entrega 2 - Adição do funcionamento básico do jogo: Geração da janela a partir da utilização de JFrame e derivados;Recebimento dos inputs e movimentação do player; Introdução do modelo de Herança do projeto (com GameObjects e etc);  Player, seus sprites e sua movimentação; texturas; geração de mapa; bomba e explosão.


## :busts_in_silhouette: Integrantes 

- Bárbara Fernandes Dias Bueno - 13679530
- Carlos Maria Engler Pinto - 5104641
- João Felipe Pereira Carvalho - 11808189
- João Pedro Bassetti Freitas - 13903411
- Juliana Mitie Hosne Nakata - 13679544
- Laura Fernandes Montenegro - 13862796

