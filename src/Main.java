import javax.swing.JFrame;
public class Main{
    public static void main(String[] args) {
        
        // Definições da janela do jogo
        JFrame window = new JFrame();
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setResizable(false);
        window.setTitle("Bomberman"); //window title

        // Adiciona o painel do jogo na janela
        Panel gamePanel = new Panel();
        window.add(gamePanel);

        // Ajusta o tamanho da janela para o tamanho do painel
        window.setLocationRelativeTo(null);
        window.pack();
        window.setVisible(true);

        // Inicia o jogo
        gamePanel.startGameThread(); 
    }
}
