import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class Player extends GameObject{

    Panel gamePanel;
    Keyhandler KeyH;
    int bombDelay;
    int bombForce;
    int maxBomb;
    int bombCount;

    private int SwitchAnimationCounter = 0;
    private boolean Animation = false;

    private boolean movement = false;
    BufferedImage stop_up, up1, up2;
    BufferedImage stop_down, down1, down2;
    BufferedImage stop_left, left1, left2;
    BufferedImage stop_right, right1, right2;

    public Player(Panel gamePanel, Keyhandler KeyH, ID id){
        super(100, 100, id);
        this.gamePanel = gamePanel;
        this.KeyH = KeyH;
        setDefaultValues();
        gameImage();
        bombDelay = 0;
    }

    public void gameImage(){
        try{
            up1 = ImageIO.read(getClass().getResourceAsStream("/res/player/personagem_move_up1.png"));
            up2 = ImageIO.read(getClass().getResourceAsStream("/res/player/personagem_move_up2.png"));
            stop_up = ImageIO.read(getClass().getResourceAsStream("/res/player/personagem_stop_up.png"));
            left1 = ImageIO.read(getClass().getResourceAsStream("/res/player/personagem_move_left1.png"));
            left2 = ImageIO.read(getClass().getResourceAsStream("/res/player/personagem_move_left2.png"));
            stop_left = ImageIO.read(getClass().getResourceAsStream("/res/player/personagem_stop_left.png"));
            right1 = ImageIO.read(getClass().getResourceAsStream("/res/player/personagem_move_right1.png"));
            right2 = ImageIO.read(getClass().getResourceAsStream("/res/player/personagem_move_right2.png"));
            stop_right = ImageIO.read(getClass().getResourceAsStream("/res/player/personagem_stop_right.png"));
            down1 = ImageIO.read(getClass().getResourceAsStream("/res/player/personagem_move_down1.png"));
            down2 = ImageIO.read(getClass().getResourceAsStream("/res/player/personagem_move_down2.png"));
            stop_down = ImageIO.read(getClass().getResourceAsStream("/res/player/personagem_stop_down.png"));
        } catch (IOException e){
            e.printStackTrace();
        }

    }

    public void setDefaultValues(){
        if (id == ID.Player1){
            setX(96);
            setY(140);
            setVelX(5);
            setVelY(5);
        }
        else if (id == ID.Player2){
            setX(200);
            setY(200);
            setVelX(5);
            setVelY(5);
        }

        bombForce = 2;
        direction = "right";
        solidArea = new Rectangle(8, 64, 12, 12);
        maxBomb = 3;
        bombCount = 0;
    }

    public void update(){
        bombDelay--;

        if(KeyH.up || KeyH.down || KeyH.left || KeyH.right) {
            movement = true;

            ColisionOn = false;
           // gamePanel.colision.checkCollision(this);

            if (KeyH.up) direction = "up";
            if (KeyH.down) direction = "down";
            if (KeyH.right) direction = "right";
            if (KeyH.left) direction = "left";
        }
        else movement = false;

        if(!ColisionOn && movement){
            switch (direction){
                case "up": y -= velY; break;
                case "down": y += velY; break;
                case "right": x += velX; break;
                case "left": x -= velX; break;
            }
        }
        
        if(KeyH.bomb && bombDelay <= 0 && bombCount < maxBomb){
            bombCount++;
            gamePanel.handler.addObject(new Bomb(x, (y + gamePanel.tileSize + 7), ID.Bomb, this));
            bombDelay = 10;
        }

        SwitchAnimationCounter++;
        if(SwitchAnimationCounter > 15){
            if(Animation) Animation = false;
            else Animation = true;
            SwitchAnimationCounter = 0;
        }

    }

    public void draw(Graphics2D g2){
        BufferedImage image = null;
        switch (direction){
            case "up":
                if(movement) {
                    if(Animation) image = up2;
                    else image = up1;
                }
                else image = stop_up;
                break;
            case "down":
                if(movement) {
                    if(Animation) image = down2;
                    else image = down1;
                }
                else image = stop_down;
                break;
            case "left":
                if(movement) {
                    if(Animation) image = left2;
                    else image = left1;
                }
                else image = stop_left;
                break;
            case "right":
                if(movement) {
                    if(Animation) image = right2;
                    else image = right1;
                }
                else image = stop_right;
                break;
        }
        g2.drawImage(image, x, y, gamePanel.tileSize, 2*gamePanel.tileSize, null);
    }
}
