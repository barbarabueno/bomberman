public class CollisionDetect {
    Panel gamePanel;

    public CollisionDetect(Panel gamePanel){
        this.gamePanel = gamePanel;
    }

    public void checkCollision(GameObject GO){
        int up = GO.y + GO.solidArea.y;
        int left = GO.x + GO.solidArea.x;
        int down = GO.y + GO.solidArea.y + GO.solidArea.height;
        int right = GO.x + GO.solidArea.x + GO.solidArea.width;

        int upRow = up/gamePanel.tileSize;
        int leftCol = left/gamePanel.tileSize;
        int downRow = down/gamePanel.tileSize;
        int rigthCol = right/gamePanel.tileSize;

        int OptionOne, OptionTwo;

        switch (GO.direction){
            case "up":
                upRow = (up - GO.velY)/gamePanel.tileSize;
                OptionOne = gamePanel.map.map[leftCol][upRow];
                OptionTwo = gamePanel.map.map[rigthCol][upRow];
                if(gamePanel.map.BackGroundItens[OptionOne].colision || gamePanel.map.BackGroundItens[OptionTwo].colision) GO.ColisionOn = true;
            case "down":
                downRow = (down + GO.velY)/gamePanel.tileSize;
                OptionOne = gamePanel.map.map[leftCol][downRow];
                OptionTwo = gamePanel.map.map[rigthCol][downRow];
                if(gamePanel.map.BackGroundItens[OptionOne].colision || gamePanel.map.BackGroundItens[OptionTwo].colision) GO.ColisionOn = true;
            case "left":
                leftCol = (left - GO.velX)/gamePanel.tileSize;
                OptionOne = gamePanel.map.map[leftCol][upRow];
                OptionTwo = gamePanel.map.map[leftCol][downRow];
                if(gamePanel.map.BackGroundItens[OptionOne].colision || gamePanel.map.BackGroundItens[OptionTwo].colision) GO.ColisionOn = true;
            case "right":
                rigthCol = (right + GO.velX)/gamePanel.tileSize;
                OptionOne = gamePanel.map.map[rigthCol][upRow];
                OptionTwo = gamePanel.map.map[rigthCol][downRow];
                if(gamePanel.map.BackGroundItens[OptionOne].colision || gamePanel.map.BackGroundItens[OptionTwo].colision) GO.ColisionOn = true;
            }
        }

}


