import java.awt.Graphics2D;

public class Explosion extends GameObject {

    static int explosionTime = 1;
    static int size = 40;
    double createTime;
    Bomb bomb;

    public Explosion(Bomb bomb) {
        super(bomb.x, bomb.y, ID.Explosion);
        this.bomb = bomb;
        createTime = System.currentTimeMillis();
    }

    @Override
    public void update() {
        if (System.currentTimeMillis() - createTime >= explosionTime * 1000){
            bomb.player.gamePanel.handler.removeObject(this);
            //player.bombCount--;
        }

    }

    @Override
    public void draw(Graphics2D g) {
        g.setColor(java.awt.Color.red);
        for(int i = x - bomb.player.bombForce * bomb.player.gamePanel.tileSize; i < x + (bomb.player.bombForce + 1)* bomb.player.gamePanel.tileSize; i += bomb.player.gamePanel.tileSize){
            g.setColor(java.awt.Color.red);
            g.fillOval(i, y, size, size);
            g.setColor(java.awt.Color.yellow);
            g.fillOval(i, y, size - 10, size - 10);
        }
        for(int i = y - bomb.player.bombForce * bomb.player.gamePanel.tileSize; i < y + (bomb.player.bombForce + 1)* bomb.player.gamePanel.tileSize; i += bomb.player.gamePanel.tileSize){
            g.setColor(java.awt.Color.red);
            g.fillOval(x, i, size, size);
            g.setColor(java.awt.Color.yellow);
            g.fillOval(x, i, size - 10, size - 10);

        }

    }
    
}
