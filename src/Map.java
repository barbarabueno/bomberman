import javax.imageio.ImageIO;
import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Map extends GameObject {
    public int map[][];

    Tile BackGroundItens[];
    Panel panel;
    public Map(String mapFile, Panel panel) {
        super(0, 0, ID.Map);
        this.panel = panel;

        BackGroundItens = new Tile[10];
        
        map = new int[panel.maxScreenLines][panel.maxScreenColumns];
        loadMap(mapFile);
        shuffleMap(map);
        getImage();
    }

    public void getImage(){
        try{
            BackGroundItens[0] = new Tile();
            BackGroundItens[0].image = ImageIO.read(getClass().getResourceAsStream("/res/scenario/Chao.png"));
            BackGroundItens[0].colision = false;

            BackGroundItens[1] = new Tile();
            BackGroundItens[1].image = ImageIO.read(getClass().getResourceAsStream("/res/scenario/Obstacules.png"));
            BackGroundItens[1].colision = true;

            BackGroundItens[2] = new Tile();
            BackGroundItens[2].image = ImageIO.read(getClass().getResourceAsStream("/res/scenario/Wall.png"));
            BackGroundItens[2].colision = true;
        } catch (IOException e){
            e.printStackTrace();
        }
    }

public void loadMap(String mapString){
        try{
            InputStream is = getClass().getResourceAsStream(mapString);
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            int col = 0;
            int row = 0;
            while(col < panel.maxScreenLines && row < panel.maxScreenColumns){
                String line = br.readLine();
                String numbers[] = line.split(" ");
                while(col < panel.maxScreenColumns){
                    int num = Integer.parseInt(numbers[col]);
                    
                    map[row][col] = num;
                    col++;
                }
                
                if(col == panel.maxScreenColumns){
                    col = 0;
                    row++;
                }
            }

            br.close();

        } catch (Exception e){
            System.out.println("NÃO CONSEGUI ABRIR O ARQUIVOOOOOO.");
            System.out.println(e);
            e.printStackTrace();
        }
    }

    public void shuffleMap (int[][] map) {
        int row = 0;
        int N;
        while (row < panel.maxScreenLines){
            N = howManyBlocks(map[row]);
            Shuffle(map[row], N);
            row++;
        }
    }

    private int howManyBlocks (int[] row){
        int col = 0;
        int N = 0;
        while (col < panel.maxScreenColumns){
            if (row[col] == 1) N++;
            col++;
        }
        if (N > 7) return 1;
        else if ((N > 5) && (N <= 7)) return 2;
        else if ((N > 3) && (N <= 5)) return 3;
        else return 4;
    }

    public void Shuffle (int[] row, int N) {
        int col = 0;
        while (N > 0){
            if (row[col] == 0) {
                row[col] = 2;
                N--;
            }
            col++;
        }
        
        Random rnd = ThreadLocalRandom.current();
        for (int i = panel.maxScreenLines-1; i > 0; i--){
            int index = rnd.nextInt(i + 1);
            // Simple swap
            int a = row[index];
            row[index] = row[i];
            row[i] = a;
        }
    }


    @Override
    public void update() {
    }

    @Override
    public void draw(Graphics2D g) {
        int XPanel = 0;
        int YPanel = 0;

        for(int i = 0; i < panel.maxScreenLines; i++){
            for(int j = 0; j < panel.maxScreenColumns; j++){
                if(map[i][j] == 0){
                     // Ground
                    g.drawImage(BackGroundItens[0].image, XPanel, YPanel, panel.tileSize, panel.tileSize, null);
                }
                if(map[i][j] == 1){
                    // Obstacules
                    g.drawImage(BackGroundItens[1].image, XPanel, YPanel, panel.tileSize, panel.tileSize, null);
                }
                else if(map[i][j] == 2){
                    // Wall
                    g.drawImage(BackGroundItens[2].image, XPanel, YPanel, panel.tileSize, panel.tileSize, null);;
                }
                XPanel += panel.tileSize;
            }
            XPanel = 0;
            YPanel += panel.tileSize;
        }
    
        
    }
    
    
}
