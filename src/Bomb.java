import java.awt.Color;
import java.awt.Graphics2D;

public class Bomb extends GameObject{
    
    public static int explosionTime = 3;
    public double createTime;
    Player player;

    public Bomb(int x, int y, ID id, Player player){
        super(x, y, id);
        setVelX(0);
        setVelY(0);
        createTime = System.currentTimeMillis();
        this.player = player;

    }

    @Override
    public void update() {
        if (System.currentTimeMillis() - createTime >= explosionTime * 1000){
            player.gamePanel.handler.removeObject(this);
            player.gamePanel.handler.addObject(new Explosion(this));
            player.bombCount--;
        }

        x += velX;
        y += velY;
    }

    @Override
    public void draw(Graphics2D g2) {
        g2.setColor(Color.black);
        g2.fillOval(x, y, 40, 40);
    }
}
