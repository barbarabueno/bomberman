import javax.swing.JPanel;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Color;

public class Panel extends JPanel implements Runnable {
    //defines the tile's characteristics -> singular unity that composes the panel
    final int originalTileSize = 16; //16*16 pixels
    final int scale = 3;
    public final int tileSize = originalTileSize * scale; //actual tile size in pixels 48*48 pixels

    //defines how many tiles there will be in the panel
    final int maxScreenColumns = 13;
    final int maxScreenLines = 11;

    //defines the window size in pixels
    final int screenWidth = tileSize * maxScreenColumns; // 48*13
    final int screenHeight = tileSize * maxScreenLines; // 48*11

    // FPS (frames per second)
    final int FPS = 60;

    // Key Handler
    Keyhandler KeyH = new Keyhandler();

    // GameObject Handler 
    protected Handler handler;

    //Player
    Player player = new Player(this, KeyH, ID.Player1);
    CollisionDetect colision = new CollisionDetect(this);

    // Thread
    Thread gameThread;

    // Map
    Map map = new Map("/maps/map01.txt",this);
    
    public Panel(){

        this.setPreferredSize (new Dimension(screenWidth, screenHeight));
        this.setBackground(Color.gray);
        this.setDoubleBuffered(true);

        this.addKeyListener(KeyH); 
        this.setFocusable(true); 
    }

    public void startGameThread(){
        gameThread = new Thread(this);
        gameThread.start();
    }
    
    @Override
    public void run() {

        this.handler = new Handler();
        this.handler.addObject(map);
        this.handler.addObject(player);
        // handler.addObject(player2);
        double drawInterval = 1000000000 / FPS; // 0.01666 seconds
        double nextDrawTime = System.nanoTime() + drawInterval; 
        
        while(gameThread != null){
            update(); // update info 
            repaint(); // redraws

            try {
                double remainingTime = (nextDrawTime - System.nanoTime()) / 1000000; // miliseconds  
                
                if(remainingTime < 0) remainingTime = 0;
                
                Thread.sleep((long) remainingTime);
                nextDrawTime += drawInterval; 

            } catch (InterruptedException e) {
                e.printStackTrace();
            } 
        }
    }

    // updating character's position
    public void update(){
        this.handler.update();
    }

    public void paintComponent(Graphics g){
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D)g;
        
        this.handler.draw(g2);
        g2.dispose();
    }   
}
