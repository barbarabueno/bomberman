public enum ID {
    Player1(),
    Player2(),
    Enemy(), 
    Explosion(),
    Map(),
    FixBlock(),
    Bomb();
}
