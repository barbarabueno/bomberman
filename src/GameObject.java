import java.awt.*;

public abstract class GameObject {

    protected int x, y; //position
    protected ID id;
    protected int velX, velY; //velocity

    Rectangle solidArea;

    String direction;

    boolean ColisionOn = false;

    public GameObject(int x, int y, ID id){
        this.x = x;
        this.y = y;
        this.id = id;
    }

    public abstract void update();
    public abstract void draw(Graphics2D g);

    public void setX(int x){ this.x = x; }
    public void setY(int y){ this.y = y; }
    public void setId(ID id){ this.id = id; }
    public void setVelX(int velX){ this.velX = velX; }
    public void setVelY(int velY){ this.velY = velY; }

    public int getX(){ return x; }
    public int getY(){ return y; }
    public ID getId(){ return id; }
    public int getVelX(){ return velX; }
    public int getVelY(){ return velY; }

}
