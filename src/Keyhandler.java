import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyhandler implements KeyListener{
    public boolean up, down, left, right, bomb; 

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        int code = e.getKeyCode(); // returns integer associated with the key

        // W key
        if(code == KeyEvent.VK_W) up = true; 
        // A key
        if(code == KeyEvent.VK_A) left = true; 
        // S key
        if(code == KeyEvent.VK_S) down = true;     
        // D key
        if(code == KeyEvent.VK_D) right = true; 
        // Space key
        if(code == KeyEvent.VK_SPACE) bomb = true;
    }

    @Override
    public void keyReleased(KeyEvent e) {
        int code = e.getKeyCode(); // returns integer associated with the key

        // W key
        if(code == KeyEvent.VK_W) up = false; 
        // A key
        if(code == KeyEvent.VK_A) left = false; 
        // S key
        if(code == KeyEvent.VK_S) down = false;     
        // D key
        if(code == KeyEvent.VK_D) right = false; 
        // Space key
        if(code == KeyEvent.VK_SPACE) bomb = false;
    }
}
