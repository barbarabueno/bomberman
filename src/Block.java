import java.awt.Graphics2D;

public abstract class Block extends GameObject{

    static int size = 48;

    public Block(int x, int y, ID id) {
        super(x, y, id);
    }

    @Override
    public void update() {
    }

    @Override
    public void draw(Graphics2D g) {
        if (id == ID.FixBlock){
            g.setColor(java.awt.Color.BLUE);
            g.fillRect(x, y, size, size);
        }
    }
    
}
